Minimum requirements
====================

- A C compiler (e.g. gcc, icc, pgcc, xlc, ...)
- A Fortran compiler (e.g. gfortran, ifort, pgif90, xlf, ...)
- A recent Python interpreter (Python 2 >= 2.7.13 or Python 3 >= 3.5.1)
- GNU Make >= 2.80 (other versions may work but have not been tested)

A basic installation would be:

.. code:: bash
   
   mkdir build 
   cd build 
   cmake ../
   make -j

and then running it would be

.. code:: bash
   
   bin/esl-demo.X <input file>

by default, the input-file will be read from `sample.inp`.


MPI Support
-----------

Forcing MPI support is done with

.. code:: bash
   
   cmake ../ -DWITH_MPI=On

if you get undefined references to MPI_Get_library_version you have a very old MPI implementation.
You are recommended to update your MPI version, or alternatively do:

.. code:: bash
   
   FFLAGS="-DOLDMPI" cmake ../ -DWITH_MPI=on


ELSI Support
------------
To forcefully enable ELSI support (by default it will try and determine if it is present)

.. code:: bash

   cmake ../ -DWITH_ELSI=On


Shared libraries
-----------------
Libraries may be built in shared-mode (`.so`)

.. code:: bash
   
   cmake ../ -DBUILD_SHARED_LIBS=on


API documentation
-----------------

.. code:: bash
   
   cmake ../ -DWITH_DOC=on
